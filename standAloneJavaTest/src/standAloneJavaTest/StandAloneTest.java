package standAloneJavaTest;

import javax.swing.JOptionPane;

import myJarTest.JarTest;


public class StandAloneTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null, "main", "standAloneJavaTest", JOptionPane.INFORMATION_MESSAGE);
		JarTest myTest = new JarTest();
		
		int val = myTest.runExternalJarMethod();
		String outMsg = "value returned from runExternalJarMethod = " + val;
		JOptionPane.showMessageDialog(null, outMsg, "standAloneJavaTest", JOptionPane.INFORMATION_MESSAGE);
	}

}
//