package myJarTest;

import javax.swing.JOptionPane;

public class JarTest {
	
	int jarCounter = 100; // initial value
	public JarTest()
	{
		jarCounter = 200; //value set in constructor
		JOptionPane.showMessageDialog(null, "constructor", "JarTest", JOptionPane.INFORMATION_MESSAGE);
	}
	public int runExternalJarMethod()
	{
		jarCounter++;
		JOptionPane.showMessageDialog(null, "runExternalJarMethod", "JarTest", JOptionPane.INFORMATION_MESSAGE);
		return jarCounter;
	}

}
