package standAloneMqttTest;



import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.util.Locale;

import javax.swing.JOptionPane;

public class StandAloneMqttTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		executeTestMethodJar();
	}

	
	
	public static int executeTestMethodJar()
	{
		JOptionPane.showMessageDialog(null, "executeTestMethodJar", "compMqttRealized", JOptionPane.INFORMATION_MESSAGE);
		//writeText("testFromProvider");

		Locale locale;
		locale = java.util.Locale.getDefault();
		String message = locale.getDisplayCountry() + "\n" + locale.getDisplayLanguage() + "\n" + locale.getDisplayName() + "\n";
		message += locale.getDisplayScript() + "\n" + locale.getDisplayVariant() + "\n" + locale.getISO3Country() + "\n";
		message += locale.getISO3Language()  + "\n" + locale.getLanguage() + "\n" + locale.getScript()  + "\n";
		message += locale.getVariant()  + "\n" + locale.toLanguageTag()  + "\n" + locale.toString();
		JOptionPane.showMessageDialog(null, message, "compMqttRealized", JOptionPane.INFORMATION_MESSAGE);
		
		/*test code ****************************************************/
		String broker = "tcp://127.0.0.1:1884";
		String clientId = "myClient";
		int qos = 2;
		JOptionPane.showMessageDialog(null, "1. in thread e", "compMqttRealized: ", JOptionPane.INFORMATION_MESSAGE);

		try {

			IMqttClient myClient = new MqttClient(broker, clientId);

			JOptionPane.showMessageDialog(null, "2. create client", "compMqttRealized: ", JOptionPane.INFORMATION_MESSAGE);

			MqttConnectOptions options = new MqttConnectOptions();
			options.setAutomaticReconnect(true);
			options.setCleanSession(true);
			options.setConnectionTimeout(10);
			myClient.connect(options);

			myClient.publish(
					"topic",
					"payload".getBytes(UTF_8),
					2, // QoS = 2
					false);
			JOptionPane.showMessageDialog(null, "setting options", "compMqttRealized", JOptionPane.INFORMATION_MESSAGE);
			myClient.close();
			////			} catch (MqttException e) {
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e.toString(), "mqtt exception", JOptionPane.ERROR_MESSAGE);
		}
		/*end of test code *********************************************/

		return 1;
	}
	
}
