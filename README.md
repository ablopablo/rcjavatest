# rcJavaTest repo


test repo to demonstrate use of realized components in BridgePoint 

project built using Bridgepoint v7.3.0.202210020157

## rcJava (xtUML)
---
Bridgepoint project with very basic structure. The following components are defined.

### compMain
- Simple component with test functions to exercise the interfaces.

### compRealized
 - The realized component

### compMqttRealized
 - Realized component to make use of the paho mqtt library

## myJarTest (test java library)
---
Basic java library with method. Builds within eclipse and library copied to a location for use with the rcJava model

## standAloneJavaTest (harness to exercise myJarTest)
---
application to demonstrate/test the myJarTest library is operational.

<br><br>

# Testing progress
<b>Setup of xtUML project</b>

## Jar location
I have tried adding the jar library to the 'Module Path' and 'Classpath' areas in the dialog
<br>
![jar library locations](jarLocation.png "Jar location")
<br>
When using the 'Modulepath' the Realized Component java code will not build - it cannot resolve the imported library.
When using the 'Classpath' the Realized component java builds without error & enables the verifier to run the component.

## compRealized - Testing normal port call
2 executions of the function highlighted shows the counter in the realized component (compRealized) is incrementing
<br>
![normal method execution working](01_running_testTestMethod.png "normal port call")
<br>

## compRealized - Testing use of realized component with imported jar
Single call to the test function indicates that the verifier has failed to complete the function execution. 
No error/warning messages are recorded in the .log file.
Subsequent exections (not shown here) of the (previously working) testTestMethod do not work.
<br>
![jar test method execution failed](02_execute_testTestMethodJar_failed.png "port call exercising imported jar")
<br>


## compRealized - Testing use of realized component with imported jar with .class folder
Starting by copying the folder myJarTest/JarTest.class from the myJarTest project build directory, into the rcJava/javasrc/bin directory & keeping the library inclusion in the Classpath config in eclipse.
Single call to the test function indicates that the xternal library executes as expected.
<br>
![jar test method execution working](03_execute_testTestMothodJar_working.png "working port call exercising imported jar")
<br>

```mermaid
sequenceDiagram
    compMain->>+compRealized: testTestMethod()
    compRealized->>compRealized: testTestMethod()
    compRealized->>-compMain: return and prints value
    compMain->>+compRealized: testTestMethodJar()
    compRealized->>+compRealizedJar: executes external jar method
    compRealizedJar->>-compRealized: external return
    compRealized->>-compMain: return and prints value
```

---

## Addition of mqtt component
Introduced another realized component making use of the same interface (TestIf), but implementing calls to the paho mqtt library.
The version of the paho library i am using is: <br>
org.eclipse.paho.client.mqttv3_1.2.1.jar
## compMqttRealized - Testing of the normal port call 
This works in the same way as the compRealized
## compMqttRealized - Testing use of realized component with imported jar
Same result as with compRealized.
## compMqttRealized - Testing use of realized component with imported jar with .class folder
Extracted the paho mqtt library and copied the contents into the /javasrc/bin folder
Upon execution of the testMqttMethodJar function in the model, the following exception is presented.
<br>
![mqtt exception error](mqtt_exception.png "exception error")
<br>

```mermaid
sequenceDiagram
    compMain->>+compMqttRealized: testTestMethod()
    compMqttRealized->>compMqttRealized: testTestMethod()
    compMqttRealized->>-compMain: return and prints value
    compMain->>+compMqttRealized: testTestMethodJar()
    compMqttRealized->>+compMqttRealizedJar: executes external jar method
    compMqttRealizedJar->>compMqttRealizedJar: error
```