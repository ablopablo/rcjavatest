# rcJava

test project to exercise realized components

project built using Bridgepoint v7.3.0.202210020157

## Model Structure

### TestIf
Interface to be used in test

### compMain
Simple component with a single function which calls the the test method in the TestIf

### compRealized
The realized component

---

## Java Code
Realized component code in the javasrc/library folder
Interface defined in the javasrc/interfaces folder

# Model execution
Using the verifier, uncheck 'Run Deterministically' and uncheck 'Enable Simulated Time'