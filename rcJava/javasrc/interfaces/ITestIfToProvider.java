package interfaces;

import org.xtuml.bp.core.ComponentInstance_c;


public interface ITestIfToProvider {
	public int executeTestMethod(ComponentInstance_c senderReceiver);
	public int executeTestMethodJar(ComponentInstance_c senderReceiver);
}
