package library;


//import lib.BPBoolean;
//import lib.BPInteger;
//import lib.BPFloat;
//import lib.BPString;
//import lib.LOG;
import interfaces.ITestIfFromProvider;
import interfaces.ITestIfToProvider;
import org.xtuml.bp.core.ComponentInstance_c;
import org.xtuml.bp.core.CorePlugin;


import java.io.FileWriter;  // Import the File class
import java.io.IOException;  // Import the IOException class to handle errors
import java.util.Date;
import java.text.SimpleDateFormat;


//import org.eclipse.paho.client.mqttv3.*;
//import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import javax.swing.JOptionPane;

import myJarTest.JarTest;

public class compRealized implements ITestIfToProvider
{
//	public static MqttConnection mqttCon;
	boolean isInitialised = false; 
	public static String textVal = "empty";
	public int counter = 0;
	
	private ITestIfFromProvider ifTestFromPort = null;
	
	public compRealized(ITestIfFromProvider port1)
	{

//		System.gc();
		ifTestFromPort = port1;
		isInitialised = true;
		//writeText("initialised HostPlatform");
	}
		
	@Override
	public int executeTestMethod(ComponentInstance_c senderReceiver)
	{
		JOptionPane.showMessageDialog(null, "executeTestMethod", "compRealized", JOptionPane.INFORMATION_MESSAGE);
		ifTestFromPort.testResponse(senderReceiver);
		
		counter++;
		return counter;
	}
	
	public int executeTestMethodJar(ComponentInstance_c senderReceiver)
	{
		JarTest myTest = new JarTest();	
		counter = myTest.runExternalJarMethod();
		return counter;
	}
	
	
	private void writeText(String msg)
	{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		
		JOptionPane.showMessageDialog(null, msg, "writeText: ", JOptionPane.INFORMATION_MESSAGE);
		//System.out.println(formatter.format(date));
		String dateString = formatter.format(date);
		 try {
		      FileWriter myWriter = new FileWriter("/home/paul/comHome/BridgePoint/workspaces/servMgr_git_repo/servicesmanager/servicesManager/filename.txt", true);
		      myWriter.write(dateString);
		      myWriter.write(msg);
		      myWriter.write("\r\n");
		      myWriter.close();
		      System.out.println("Successfully wrote to the file.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		      CorePlugin.out.println("writeText error.");
		      JOptionPane.showMessageDialog(null, "error", "InfoBox: ", JOptionPane.INFORMATION_MESSAGE);
		    }
	}
	
}
