package library;


//import lib.BPBoolean;
//import lib.BPInteger;
//import lib.BPFloat;
//import lib.BPString;
//import lib.LOG;
import interfaces.ITestIfFromProvider;
import interfaces.ITestIfToProvider;
import org.xtuml.bp.core.ComponentInstance_c;
import org.xtuml.bp.core.CorePlugin;


import java.io.FileWriter;  // Import the File class
import java.io.IOException;  // Import the IOException class to handle errors
import java.util.Date;
import java.util.Locale;
import java.text.SimpleDateFormat;


import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import javax.swing.JOptionPane;

import static java.nio.charset.StandardCharsets.UTF_8;



public class compMqttRealized implements ITestIfToProvider
{
	//	public static MqttConnection mqttCon;
	boolean isInitialised = false; 
	public static String textVal = "empty";
	public int counter = 0;

	private ITestIfFromProvider ifTestFromPort = null;

	public compMqttRealized(ITestIfFromProvider port1)
	{

		//		System.gc();
		ifTestFromPort = port1;
		isInitialised = true;
		//writeText("initialised HostPlatform");
	}

	@Override
	public int executeTestMethod(ComponentInstance_c senderReceiver)
	{
		JOptionPane.showMessageDialog(null, "executeTestMethod", "compMqttRealized", JOptionPane.INFORMATION_MESSAGE);
		ifTestFromPort.testResponse(senderReceiver);

		counter++;
		return counter;
	}

	public int executeTestMethodJar(ComponentInstance_c senderReceiver)
	{
		JOptionPane.showMessageDialog(null, "executeTestMethodJar", "compMqttRealized", JOptionPane.INFORMATION_MESSAGE);
		//writeText("testFromProvider");

		Locale locale;
		locale = java.util.Locale.getDefault();
		String message = locale.getDisplayCountry() + "\n" + locale.getDisplayLanguage() + "\n" + locale.getDisplayName() + "\n";
		message += locale.getDisplayScript() + "\n" + locale.getDisplayVariant() + "\n" + locale.getISO3Country() + "\n";
		message += locale.getISO3Language()  + "\n" + locale.getLanguage() + "\n" + locale.getScript()  + "\n";
		message += locale.getVariant()  + "\n" + locale.toLanguageTag()  + "\n" + locale.toString();
		JOptionPane.showMessageDialog(null, message, "compMqttRealized", JOptionPane.INFORMATION_MESSAGE);
		/*test code ****************************************************/
		String broker = "tcp://127.0.0.1:1884";
		String clientId = "myClient";
		int qos = 2;
		JOptionPane.showMessageDialog(null, "1. in thread e", "compMqttRealized: ", JOptionPane.INFORMATION_MESSAGE);

		try {

			IMqttClient myClient = new MqttClient(broker, clientId);

			JOptionPane.showMessageDialog(null, "2. create client", "compMqttRealized: ", JOptionPane.INFORMATION_MESSAGE);

			MqttConnectOptions options = new MqttConnectOptions();
			options.setAutomaticReconnect(true);
			options.setCleanSession(true);
			options.setConnectionTimeout(10);
			myClient.connect(options);

			myClient.publish(
					"topic",
					"payload".getBytes(UTF_8),
					2, // QoS = 2
					false);
			JOptionPane.showMessageDialog(null, "setting options", "compMqttRealized", JOptionPane.INFORMATION_MESSAGE);
			myClient.close();
			////			} catch (MqttException e) {
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e.toString(), "mqtt exception", JOptionPane.ERROR_MESSAGE);
		}
		/*end of test code *********************************************/

		return counter;
	}


	private void writeText(String msg)
	{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

		JOptionPane.showMessageDialog(null, msg, "writeText: ", JOptionPane.INFORMATION_MESSAGE);
		//System.out.println(formatter.format(date));
		String dateString = formatter.format(date);
		try {
			FileWriter myWriter = new FileWriter("/home/paul/comHome/BridgePoint/workspaces/servMgr_git_repo/servicesmanager/servicesManager/filename.txt", true);
			myWriter.write(dateString);
			myWriter.write(msg);
			myWriter.write("\r\n");
			myWriter.close();
			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
			CorePlugin.out.println("writeText error.");
			JOptionPane.showMessageDialog(null, "error", "InfoBox: ", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
